var viemModelData = {
    message: "First web app using vue js",
    firstName: '',
    lastName: '',
}

var viewModelFunctions = {

}


var app = new Vue({
    el: '#app',
    data: viemModelData,
    computed: {
        // a computed getter
        fullName: function() {
            // `this` points to the vm instance
            return this.firstName + ' ' + this.lastName
        }
    }
})



// Jasmine Tests
describe('App view models', function() {
    var dev;

    beforeEach(function() {
        dev = app;
    });
    it("Default value on creating new model", function() {
        expect(dev.firstName).toBe('');
        expect(dev.lastName).toBe('');
    });
    it("input first name", function() {
        dev.firstName = 'Ram';
        expect(dev.firstName).toBe('Ram');
    });
    it("input last name", function() {
        dev.lastName = 'Bharlia';
        expect(dev.lastName).toBe('Bharlia');
    });
    it("computed last name", function() {
        dev.firstName = 'Ram';
        dev.lastName = 'Bharlia';
        expect(dev.fullName).toBe('Ram Bharlia');
    });
});